# A) 
#pragma once

#include <atomic>
#include <mutex>

// Test-And-Set spinlock
class TASSpinLock {
public:
    void Lock() {
        while (locked_.exchange(true, std::memory_order_acq_rel)) {
            std::this_thread::yield();
        }
    }
    void Unlock() {
        locked_.store(false, std::release());
    }
    
private:
    std::atomic<bool> locked_{false};
};

# 1) 
Для корректной работы спинлока нужно, чтобы несколько потоков не могли одновременно попасть в критическую секцию и чтобы не возникало дедлока (в данном случае он может возникнуть, если все потоки будут ждать в цикле while, но ни один из них не будет в это время находиться в критической секции и, соответственно, не сможет вызвать Unlock(), чтобы их разблокировать)
Два потока могут одновременно находиться в кртитческой секции, если для обоих в момент проверки значение, которое вернула операция locked_.exchange(true, std::memory_order_acq_rel), окажется равным false. 
Потоки окажутся в дедлоке, если для каждого из них locked_.exchange(true, std::memory_order_acq_rel) вернет true.

# 2) 
Пусть есть потоки T1 и T2. Пусть в T1 locked_.exchange(true, std::memory_order_acq_rel) вернула false, и он смог попасть в критическую секцию. В потоке T2 locked_.exchange(true, std::memory_order_acq_rel) будет возвращать true до тех пор, пока в потоке T1 не будет вызван Unlock(), если в потоке T2 в момент прверки виден результат записи значения true в locked_, сделанной в потоке T1.

Пусть ни один из потоков в программе не находится в критической секции, тогда для одного из них locked_.exchange(true, std::memory_order_acq_rel) гарантированно вернет false: если запись, сделанная в последовательности выполнения программы, видна последующему чтению, то записанное в locked_ при вызове Unlock() значение false будет прочитано и возвращено при вызове locked_.exchange(true, std::memory_order_acq_rel).
# 3)
acquire-release memory order на locked_ в операции exchange гарантирует, что все записи в переменную locked_ с acquire-release или release memory order, которые выполнились до exchange, видны в ней, в также все чтения переменной locked_ c acquire-release или acquire memory order видят запись, которая произошла при вызове exchange. В таком случае exchange в потоке T2 читает значение locked_, которое перезаписал поток T1, если exchange в потоке T2 находится после exchange в потоке T1 в данной последовательности исполнения. Тогда два потока не могут одновременно находиться в крититческой секции, ведь при проверке условия цикла while для одного ииз них условие гарантированно будет не выполнено.

Release-семантика locked_.store(false, std::release()) делает результат записи видимым для всех чтений переменной locked_, имеющих семантику acquire и для RMW-операций с acquire-release семантикой и выполняющихся после.

# B)

#pragma once

#include <atomic>
#include <vector>

// Single-Producer/Single-Consumer Fixed-Size Ring Buffer (Queue)

template <typename T>
class SPSCRingBuffer {
public:
    explicit SPSCRingBuffer(const size_t capacity)
        : buffer_(capacity + 1) {
    }
    bool Publish(T element) {
        const size_t curr_head = head_.load(std::memory_order_acquire);
        const size_t curr_tail = tail_.load(std::memory_order_relaxed);
        if (Full(curr_head, curr_tail)) {
            return false;
        }
        buffer_[curr_tail] = element;
        tail_.store(Next(curr_tail), std::memory_order_release);
        return true;
    }
    bool Consume(T& element) {
        const size_t curr_head = head_.load(std::memory_order_relaxed);
        const size_t curr_tail = tail_.load(std::memory_order_acquire);
        if (Empty(curr_head, curr_tail)) {
            return false;
        }
        element = buffer_[curr_head];
        head_.store(Next(curr_head), std::memory_order_release);
        return true;
    }

private:
    bool Full(const size_t head, const size_t tail) const {
        return Next(tail) == head;
    }
    bool Empty(const size_t head, const size_t tail) const {
        return tail == head;
    }
    size_t Next(const size_t slot) const {
        return (slot + 1) % buffer_.size();
    }

private:
    std::vector<T> buffer_;
    std::atomic<size_t> tail_{0};
    std::atomic<size_t> head_{0};
};
# 1)
Циклический буфер single-producer single-consumer, он рассчитан на работу с двумя потоками: один выполняет Publish, другой - Consume. Нужно, чтобы Consume в потоке-consumer-е видел элемемнт, который добавил Publish в потоке-producer-e, который был в последоваельности исполнения до него. Нужно, чтобы buffer_[curr_tail] = element была связана с element = buffer_[curr_head] отношением happens-before в Consume, который следует за Publish в последовательности исполнения, иначе, если до добавления elelment буфер был пуст, Consume вернет false, тогда как должен вернуть true и сохранить в передаваемый по ссылке аргумент этот единственный элемент.

# 2)
Если чтение переменной head_ видит запись в нее, которая предшествовала чтению в последовательности исполнения, и наоборот, запись в curr_head видна последующим чтениям, и то же самое верно для tail_, проверки на пустоту, заполненность, а также добавлние элемента в хвост и взятие элемента из начала работают корректно.

# 3)
Опеация чтения tail_ в в Publish не требует сигхронизации с операциями во втором потоке: запись в tail_ происходит только в потоке, который всегда вызывает Publish, но в нем запись и чтенте tail_ согласованы отношением program_order. Аналогично для head_ в в Consume. Запись tail_.store(Next(curr_tail), std::memory_order_release) в потоке, вызывающем Publish, видна для tail_.load(std::memory_order_relaxed) в чтении, следующем за ней последовательности исполнения в потоке, вызывающем Consume. Аналогично для head_.store(Next(curr_head), std::memory_order_release) и head_.load(std::memory_order_acquire). В таком случае  buffer_[curr_tail] = element -> (program order) -> tail_.store(Next(curr_tail), std::memory_order_release) -> (suncronizes with) -> tail_.load(std::memory_order_acquire) -> (program order) -> element = buffer_[curr_head].

# C)
#pragma once

#include <atomic>
#include <mutex>
#include <functional>

template <typename T>
class LazyValue {
    using Factory = std::function<T*()>;
public:
    explicit LazyValue(Factory create)
        : create_(create) {
    }
    T& Get() {
        // double checked locking pattern
        T* curr_ptr = ptr_to_value_.load(std::memory_order_acquire);
        if (curr_ptr == nullptr) {
            std::lock_guard<std::mutex> guard(mutex_);
            curr_ptr = ptr_to_value_.load(std::memory_order_relaxed);
            if (curr_ptr == nullptr) {
                curr_ptr = create_();
                ptr_to_value_.store(curr_ptr, std::memory_order_release);
            }
        }
        return *curr_ptr;
    }
    ~LazyValue() {
        if (ptr_to_value_.load() != nullptr) {
            delete ptr_to_value_;
        }
    }

private:
    Factory create_;
    std::mutex mutex_;
    std::atomic<T*> ptr_to_value_{nullptr};
};

# 1), 2)
Чтобы правильно работал double-checked locking, проверка 
curr_ptr == nullptr
вне мьютекса должна давать результат false, если какой-то поток уже вошел в критическую секцию и выполнил  
ptr_to_value_.store(curr_ptr, std::memory_order_release);
Критические секции упорядочены, поэтому вторая проверка (под мьютекссом) в самом деле вернет false, но семантика double-checked locking будет нарушена, ведь смысл в том, чтобы не брать мьютекс для проверки
Это может случиться, если проверка вне мьютекса вернет true, потому что чтение 
 T* curr_ptr = ptr_to_value_.load(std::memory_order_acquire);
 не будет видеть запись 
 ptr_to_value_.store(curr_ptr, std::memory_order_release);
 Чтобы чтение видело данную запись, нужна acquire-release семантика атомарных оперций в данном случае.

