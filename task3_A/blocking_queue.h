#include <condition_variable>
#include <thread>
#include <condition_variable>
#include <exception>
#include <deque>
#include <atomic>

template <class T, class Container = std::deque<T>>
class BlockingQueue {
public:
    explicit BlockingQueue(const std::size_t capacity): capacity_(capacity), is_on_(true) {}

    void Put(T&& element) {
        std::unique_lock<std::mutex> lock(mtx_);
        if (!is_on_) {
            throw std::logic_error("BlockingQueue: Put: was shutdown");
        }
        while (items_.size() >= capacity_) {
            can_push_.wait(lock);
        }
        if (!is_on_) {
                throw std::logic_error("BlockingQueue: Put: was shutdown");
        }
        items_.push_back(std::move(element));
        can_pop_.notify_one();
    }

    bool Get(T& element) {
        std::unique_lock<std::mutex> lock(mtx_);
        while (items_.empty()){
            if (!is_on_){
                return false;
            }
            can_pop_.wait(lock);
        }
        element = std::move(items_.front());
        items_.pop_front();
        can_push_.notify_one();
        return true;
    }

    void Shutdown() {
        std::unique_lock<std::mutex> lock(mtx_);
        is_on_ = false;
        can_pop_.notify_all();
        can_push_.notify_all();
    }
private:
    Container items_;
    std::size_t capacity_;
    bool is_on_;
    std::condition_variable can_pop_, can_push_;
    std::mutex mtx_;
};


