#include <iostream>
#include <vector>
#include <atomic>
#include <algorithm>
#include <thread>

class ticket_spinlock {
public:
    ticket_spinlock() {
        next_free_ticket.store(0);
        owner_ticket.store(0);
    }

    void lock() {
        size_t this_thread_ticket = next_free_ticket.fetch_add(1);
        while (this_thread_ticket != owner_ticket.load()) {
            std::this_thread::yield();
        }
    }
    void unlock() {
        owner_ticket.fetch_add(1);
    }
    bool try_lock() {
        size_t owner = owner_ticket.load();
        return next_free_ticket.compare_exchange_strong(owner, owner + 1);
    }

private:
    std::atomic<size_t> next_free_ticket;
    std::atomic<size_t> owner_ticket;
};

