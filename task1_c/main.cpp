#include "ticket_spinlock.h"
ticket_spinlock spinlock;
int k = 0;
const int N = 1000;

void print_count () {
    for (int i = 0; i < N; i++) {
        if (!spinlock.try_lock()) {
            --i;
            std::this_thread::yield();
            continue;
        }
        std::cout << i;
        int v = k;
        v += 1;
        std::this_thread::yield();
        k = v;
        spinlock.unlock();
    }
}

int NUM_THREADS = 20;
int main() {
    std::function <void ()> func = print_count;
    int n = 0;
    std::vector<std::thread> threads;
    for (int i  = 0; i < NUM_THREADS; i++) {
        threads.emplace_back(func);
    }
    for (int i = 0; i < NUM_THREADS; i++) {
        threads[i].join();
    }
    std::cout << k << std::endl;
    return 0;
}
