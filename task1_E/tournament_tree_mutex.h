#include <thread>
#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <atomic>

class PetersonMutex {
public:
    PetersonMutex() {
        want[0].store(false);
        want[1].store(false);
        victim.store(0);
    }

    // Actually this move-constructor just
    // makes a copy of an object
    // because it's impossible to delete
    // object from stack.
    PetersonMutex(const PetersonMutex&& m) {
        want[0].store(m.want[0].load());
        want[1].store(m.want[1].load());
        victim.store(m.victim.load());
    }

    void Lock(const size_t thread_id) {
        size_t other = 1 - thread_id;
        want[thread_id].store(true);
        victim.store(thread_id);
        while (want[other].load() && victim.load() == (int)thread_id) {
            std::this_thread::yield();
        }
    }

    void Unlock(const size_t thread_id) {
        want[thread_id].store(false);
    }


private:
    std::array<std::atomic<bool>, 2> want;
    std::atomic<int> victim;
};

class TreeMutex {
public:
    TreeMutex(const size_t threads_num):
        size_(Pow2NotLess(threads_num)) {
        for (int i = 0; i < (int)size_; i++) {
            mutexes_.emplace_back();
        }
    }

    void Lock(size_t thread_id) {
        size_t current_mutex = FirstOnTheWay(thread_id);
        while (current_mutex != 0) {
            size_t next = GetParrent(current_mutex);
            mutexes_[next].Lock(LeftOrRightThread(current_mutex));
            current_mutex = next;
        }
    }

    void Unlock(size_t thread_id) {
        size_t current_mutex = 0;
        size_t median = size_ / 2;
        while (current_mutex < size_ - 1) {
            int next;
            if (thread_id < median) {
                next = GetLeftChild(current_mutex);
            } else {
                next = GetRightChild(current_mutex);
                thread_id -= median;
            }
            mutexes_[current_mutex].Unlock(LeftOrRightThread(next));
            current_mutex = next;
            median /= 2;
        }
    }
private:
    size_t GetParrent(const size_t current) {
         return (current - 1) / 2;
    }

    size_t GetLeftChild(const size_t current) {
        return 2 * current + 1;
    }

    size_t GetRightChild(const size_t current) {
        return 2 * current + 2;
    }

    size_t LeftOrRightThread(const size_t mutex_num) {
        return mutex_num % 2;
    }

    // First tournament's number
    // for index thread.
    size_t FirstOnTheWay(const size_t thread_num) {
        return thread_num + size_ - 1;
    }
    size_t Pow2NotLess(const size_t x) {
        size_t res = 1;
        for (; res < x; res *= 2);
        return res;
    }
    // Size is a power of 2
    // greater than num_threads
    // and nearest to it (it's a number
    // of leaves in the tree).
    size_t size_;
    std::vector<PetersonMutex> mutexes_;
};


