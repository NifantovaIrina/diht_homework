#include <thread>
#include <iostream>
#include <functional>
#include <vector>
#include <algorithm>
#include <atomic>

class PetersonMutex {
public:
    PetersonMutex() {
        want[0].store(false);
        want[1].store(false);
        victim.store(0);
    }

    // Actually this move-constructor just
    // makes a copy of an object
    // because it's impossible to delete
    // object from stack.
    PetersonMutex(const PetersonMutex&& m) {
        want[0].store(m.want[0].load());
        want[1].store(m.want[1].load());
        victim.store(m.victim.load());
    }

    void Lock(const size_t thread_id) {
        size_t other = 1 - thread_id;
        want[thread_id].store(true);
        victim.store(thread_id);
        while (want[other].load() && victim.load() == (int)thread_id) {
            std::this_thread::yield();
        }
    }

    void Unlock(const size_t thread_id) {
        want[thread_id].store(false);
    }


private:
    std::array<std::atomic<bool>, 2> want;
    std::atomic<int> victim;
};

class TreeMutex {
public:
    TreeMutex(const size_t threads_num):
        num_of_leaves_(Pow2NotLess(threads_num)) {
    }

    void lock(size_t thread_id) {/*
        size_t current_mutex = FirstFromBottom(thread_id);
        while (current_mutex != 0) {
            size_t next = GetParrent(current_mutex);
            mutexes_[next].Lock(LeftOrRightThread(current_mutex));
            current_mutex = next;
        }*/
        std::vector<size_t> way;
        GetWay(thread_id, way);
        for (size_t i = 0; i < way.size() - 1; i++) {
            mutexes_[way[i + 1]].Lock(LeftOrRightThread(way[i]));
        }
    }

    void unlock(const size_t thread_id) {
        std::vector<size_t> way;
        GetWay(thread_id, way);
        for (size_t i = way.size() - 1; i > 0; i--) {
            mutexes_[way[i]].Unlock(LeftOrRightThread(way[i - 1]));
        }
    }

private:
    bool IsRoot(const size_t current) {
        return current == 0;
    }

    size_t GetParent(const size_t current) {
         return (current - 1) / 2;
    }

    size_t LeftOrRightThread(const size_t mutex_num) {
        return mutex_num % 2;
    }

    void GetWay(const size_t thread_num, std::vector<size_t>& way) {
        size_t current = FirstFromBottom(thread_num);
        while (!IsRoot(current)) {
            way.push_back(current);
            current = GetParent(current);
        }
        way.push_back(0);
    }

    // First tournament's number
    // for index thread.
    size_t FirstFromBottom(const size_t thread_num) {
        return thread_num + num_of_leaves_ - 1;
    }
    size_t Pow2NotLess(const size_t x) {
        size_t res = 1;
        for (; res < x; res *= 2);
        return res;
    }

    size_t num_of_leaves_;
    std::vector<PetersonMutex> mutexes_{num_of_leaves_ * 2};
};

