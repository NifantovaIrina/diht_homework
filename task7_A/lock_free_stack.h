#pragma once

#include <atomic>
#include <thread>
#include <iostream>

///////////////////////////////////////////////////////////////////////

template <typename T>
class LockFreeStack {
    struct Node {
        T element_;
        std::atomic<Node*> next_;
        Node(T& element)
            : element_(element) {
            next_.store(nullptr);
        }
    };

 public:
    explicit LockFreeStack() {
    }

    ~LockFreeStack() {
        T back;
        Node* curr_top = top_;
        while (curr_top != nullptr) {
            Node* next = curr_top->next_;
            delete curr_top;
            curr_top = next;
        }
        curr_top = top_deleted_;
        while (curr_top != nullptr) {
            Node* next = curr_top->next_;
            delete curr_top;
            curr_top = next;

        }
    }

    void Push(T element) {
           Node* new_node = new Node(element);
           new_node->next_.store(top_.load());
           Node* curr_top = new_node->next_.load();
           while (!top_.compare_exchange_weak(curr_top, new_node)) {
               new_node->next_.store(curr_top);
           }
    }

    bool Pop(T& element) {
        Node* prev_top = top_.load();
        while (true) {
            if (prev_top == nullptr) {
                return false;
            }
            if (top_.compare_exchange_weak(prev_top, prev_top->next_.load())) {
                element = prev_top->element_;
                Node* curr_top = top_deleted_;
                prev_top->next_.store(curr_top);
                while (!top_deleted_.compare_exchange_weak(curr_top, prev_top)) {
                    prev_top->next_.store(curr_top);
                }
                return true;
            }
        }
    }

 private:
    std::atomic<Node*> top_{nullptr};
    std::atomic<Node*> top_deleted_{nullptr};
};

///////////////////////////////////////////////////////////////////////

template <typename T>
using ConcurrentStack = LockFreeStack<T>;

///////////////////////////////////////////////////////////////////////
