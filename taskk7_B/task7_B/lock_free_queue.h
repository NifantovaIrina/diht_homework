#pragma once

#include <thread>
#include <atomic>

///////////////////////////////////////////////////////////////////////

template <typename T, template <typename U> class Atomic = std::atomic>
class LockFreeQueue {
    struct Node {
        T element_{};
        Atomic<Node*> next_{nullptr};
        explicit Node(T element, Node* next = nullptr)
            : element_(std::move(element))
            , next_(next) {
        }

        explicit Node() {
        }
    };

 public:
    explicit LockFreeQueue() {
        Node* dummy = new Node{};
        head_.store(dummy);
        tail_.store(dummy);
        old_head_ = dummy;
    }

    ~LockFreeQueue() {
        Node* iterator = old_head_;
        while (iterator != nullptr) {
            Node* current = iterator;
            iterator = current->next_.load();
            delete current;
        }
    }

    void Enqueue(T element) {
        num_workers_.fetch_add(1);
        Node* new_tail = new Node(element);
        Node* curr_tail;
        while (true) {
            curr_tail = tail_.load();
            if (curr_tail->next_.load() == nullptr) {
                Node* tmp = nullptr;
                if (curr_tail->next_.compare_exchange_strong(tmp, new_tail)) {
                    break;
                }
            } else {
                tail_.compare_exchange_strong(curr_tail, curr_tail->next_.load());
            }
        }
        tail_.compare_exchange_strong(curr_tail, new_tail);
        num_workers_.fetch_sub(1);
    }

    bool Dequeue(T& element) {
        while (true) {
            num_workers_.fetch_add(1);
            Node* curr_head = head_.load();
            Node* curr_tail = tail_.load();
            if (curr_head == curr_tail) {
                if (curr_head->next_.load() == nullptr) {
                    if (num_workers_.load() == 1) {
                        Cleaning(curr_head);
                    }
                    num_workers_.fetch_sub(1);
                    return false;
                } else {
                    tail_.compare_exchange_strong(curr_head, curr_head->next_.load());
                }
            } else {
                if (head_.compare_exchange_strong(curr_head, curr_head->next_.load())) {
                    if (num_workers_.load() == 1) {
                        Cleaning(curr_head);
                    }
                    element = curr_head->next_.load()->element_;
                    num_workers_.fetch_sub(1);
                    return true;
                }
            }
        }
        num_workers_.fetch_sub(1);
        return false;
    }

 private:
    void Cleaning(Node* end) {
        Node* iterator = old_head_;
        while (iterator != end) {
            Node* current = iterator;
            iterator = current->next_.load();
            delete current;
        }
        old_head_ = end;
    }

    Atomic<Node*> head_{nullptr};
    Atomic<Node*> tail_{nullptr};
    Node* old_head_{nullptr};
    Atomic<size_t> num_workers_{0};
};

///////////////////////////////////////////////////////////////////////
