#pragma once

#include <iostream>
#include <condition_variable>
#include <mutex>
#include <vector>

class Semaphore {
public:
    Semaphore (const std::size_t& free_positions)
        : free_positions_(free_positions) {
    }

    void Wait() {
        std::unique_lock<std::mutex> lock(mutex_);
        while (free_positions_ == 0) {
            can_go_.wait(lock);
        }
        free_positions_--;
        
    }

    void Signal() {
        std::unique_lock<std::mutex> lock(mutex_);
        free_positions_++;
        can_go_.notify_one();
    }

private:
    size_t free_positions_;
    std::mutex mutex_;
    std::condition_variable can_go_;
};


class Robot {
public:
    Robot (const size_t legs_num) {
        sems.resize(legs_num);
        sems[0].Signal();
    }

    void Step(const size_t foot) {
        sems_[foot].Wait();
        std::cout << "foot " << foot << std::endl;
        if (foot < sems_.size() - 1) {
            sems_[foot + 1].Signal();
        } else {
            sems_[0].Signal();
        }
    }

private:
    std::vector<Semaphore> sems_;
};

