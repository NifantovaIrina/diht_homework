#pragma once

#include <iostream>
#include <condition_variable>
#include <mutex>

class Semaphore {
public:
    Semaphore (const std::size_t& free_positions)
        : free_positions_(free_positions) {
    }

    void Wait() {
        std::unique_lock<std::mutex> lock(mutex_);
        while (free_positions_ == 0) {
            can_go_.wait(lock);
        }
        free_positions_--;
        
    }

    void Signal() {
        std::unique_lock<std::mutex> lock(mutex_);
        free_positions_++;
        can_go_.notify_one();
    }

private:
    size_t free_positions_;
    std::mutex mutex_;
    std::condition_variable can_go_;
};

class Robot {
public:
    Robot()
        : semaphore_left_(1),
          semaphore_right_(0) {
    }

    void StepLeft() {
        semaphore_left_.Wait();
        std::cout << "left" << std::endl;
        semaphore_right_.Signal();
    }

    void StepRight() {
        semaphore_right_.Wait();
        std::cout << "right" << std::endl;
        semaphore_left_.Signal();
    }
private:
    Semaphore semaphore_left_;
    Semaphore semaphore_right_;
};
