#pragma once

#pragma once
#include <iostream>
#include <mutex>
#include <condition_variable>


class Robot {
public:
    Robot()
        : step_(0) {
    }

    void StepLeft() {
        std::unique_lock<std::mutex> lock(mutex_);
        while (step_ == 1) {
            can_go_.wait(lock);
        }
        std::cout << "left" << std::endl;
        step_ = 1;
        can_go_.notify_one();
    }

    void StepRight() {
        std::unique_lock<std::mutex> lock(mutex_);
        while (step_ == 0) {
            can_go_.wait(lock);
        }
        std::cout << "right" << std::endl;
        step_ = 0;
        can_go_.notify_one();
    }


private:
    std::condition_variable can_go_;
    std::mutex mutex_;
    size_t step_;
};

