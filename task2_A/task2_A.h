#pragma once
#include <mutex>
#include <condition_variable>

template <class ConditionVariable = std::condition_variable>
class CyclicBarrier {
public:
    explicit CyclicBarrier(size_t num_threads):
        demand_threads_num(num_threads), current_threads_num(0), era(0) {}

    void Pass() {
        std::unique_lock<std::mutex> lock(mutex);
        size_t current_era = era;
        current_threads_num++;
        if (current_threads_num == demand_threads_num) {
            era++;
            current_threads_num = 0;
            can_go.notify_all();
            return;
        }
        while (current_era == era) {
            can_go.wait(lock);
        }
    }

private:
    size_t demand_threads_num;
    size_t current_threads_num;
    ConditionVariable can_go;
    std::mutex mutex;
    size_t era;
};
