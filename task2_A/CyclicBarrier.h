#pragma once
#include <mutex>
#include <condition_variable>

template <class ConditionVariable = std::condition_variable>
class CyclicBarrier {
public:
    explicit CyclicBarrier(const size_t num_threads)
        : demand_threads_num_(num_threads),
          current_threads_num_(0),
          epoch_(0) {
    }

    void Pass() {
        std::unique_lock<std::mutex> lock(mutex_);
        size_t current_epoch = epoch_;
        current_threads_num_++;
        if (current_threads_num_ == demand_threads_num_) {
            epoch_++;
            current_threads_num_ = 0;
            can_go_.notify_all();
            return;
        }
        while (current_epoch == epoch_) {
            can_go_.wait(lock);
        }
    }

private:
    size_t demand_threads_num_;
    size_t current_threads_num_;
    ConditionVariable can_go_;
    std::mutex mutex_;
    size_t epoch_;
};

