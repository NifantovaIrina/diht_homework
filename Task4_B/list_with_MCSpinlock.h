#pragma once

#include <iostream>
#include <vector>
#include <atomic>
#include <algorithm>
#include <thread>
#include "arena_allocator.h"
#include<mutex>

#pragma once

#include <thread>

//////////////////////////////////////////////////////////////////////

void CpuPause() {
    // x86 architecture only
    asm volatile("rep; nop" ::: "memory");
}

void SpinLockPause() {
    // CpuPause();
    std::this_thread::yield();
}

//////////////////////////////////////////////////////////////////////


#include <iostream>
#include <atomic>
#include <thread>
#include <mutex>

//////////////////////////////////////////////////////////////////////

/* scalable and fair MCS (Mellor-Crummy, Scott) spinlock implementation
 *
 * usage:
 * {
 *   MCSSpinLock::Guard guard(spinlock); // spinlock acquired
 *   ... // in critical section
 * } // spinlock released
 */

class MCSSpinLock {
public:

    MCSSpinLock () : wait_queue_tail_(nullptr) {}
    class Guard {
    public:
        explicit Guard(MCSSpinLock& spinlock)
            : spinlock_(spinlock) {
            lock();
        }

        ~Guard() {
            unlock();
        }

        void lock() {
            // add self to spinlock queue and wait for ownership
            Guard* prev_tail = spinlock_.wait_queue_tail_.exchange(this);
            if (prev_tail != nullptr) {
                prev_tail->next_.store(this);
            } else {
                is_owner_.store(true);
            }
            while (!is_owner_.load()) {
                std::this_thread::yield();
            }
        }

        void unlock() {
            /* transfer ownership to the next guard node in spinlock wait queue
             * or reset tail pointer if there are no other contenders
             */
            auto self = this;
            if (!spinlock_.wait_queue_tail_.compare_exchange_strong(self, nullptr)) {
                while (next_.load() == nullptr) {
                  std::this_thread::yield();
                }
                next_.load()->is_owner_.store(true);
             }
       }

    private:
        MCSSpinLock& spinlock_;
        std::atomic<bool> is_owner_{false};
        std::atomic<Guard*> next_{nullptr};
    };

private:
    std::atomic<Guard*> wait_queue_tail_{nullptr};
};

template <typename T>
struct ElementTraits {
    static T Min() {
        return std::numeric_limits<T>::min();
    }
    static T Max() {
        return std::numeric_limits<T>::max();
    }
};

template <typename T>
class OptimisticLinkedSet {
 private:
    struct Node {
        T element_;
        std::atomic<Node*> next_;
        MCSSpinLock lock_{};
        std::atomic<bool> marked_{false};

        Node(const T& element, Node* next = nullptr)
            : element_(element),
              next_(next) {
        }
    };

    struct Edge {
        Node* pred_;
        Node* curr_;

        Edge(Node* pred, Node* curr)
            : pred_(pred),
              curr_(curr) {
        }
    };

 public:
    explicit OptimisticLinkedSet(ArenaAllocator& allocator)
        : allocator_(allocator), size(0) {
        CreateEmptyList();

    }

    bool Insert(const T& element) {
        while (true) {
            Edge edge = Locate(element);
            MCSSpinLock::Guard pred(edge.pred_->lock_);
            MCSSpinLock::Guard curr(edge.curr_->lock_);
            //std::unique_lock<MCSSpinLock>(edge.pred_->lock_);
            //std::unique_lock<MCSSpinLock>(edge.curr_->lock_);
            if (Validate(edge)) {
                if (edge.curr_->element_ == element) {
                    return false;
                }
                Node* new_node = allocator_.New<Node>(element, edge.curr_);
                size.fetch_add(1);
                edge.pred_->next_.store(new_node);
                return true;
            }
        }
    }

    bool Remove(const T& element) {
        while (true) {
            Edge edge = Locate(element);
            MCSSpinLock::Guard pred(edge.pred_->lock_);
            MCSSpinLock::Guard curr(edge.curr_->lock_);
//            std::unique_lock<MCSSpinLock<std::atomic>>(edge.pred_->lock_);
//            std::unique_lock<MCSSpinLock<std::atomic>>(edge.curr_->lock_);
            if (Validate(edge)) {
                if (edge.curr_->element_ != element) {
                    return false;
                }
                edge.curr_->marked_ = true;
                edge.pred_->next_.store(edge.curr_->next_);
                size.fetch_sub(1);
                return true;
            }
        }
    }

    bool Contains(const T& element) const {
        while (true) {
            Edge edge = Locate(element);
            MCSSpinLock::Guard pred(edge.pred_->lock_);
            MCSSpinLock::Guard curr(edge.curr_->lock_);
//            std::unique_lock<MCSSpinLock<std::atomic>>(edge.pred_->lock_);
//            std::unique_lock<MCSSpinLock<std::atomic>>(edge.curr_->lock_);
            if (Validate(edge)) {
                bool res = edge.curr_->element_ == element;
                return res;
            }
        }
    }

    size_t Size() const {
        return size;
    }

 private:
    void CreateEmptyList() {
        head_ = allocator_.New<Node>(ElementTraits<T>::Min());
        head_->next_ = allocator_.New<Node>(ElementTraits<T>::Max());
    }

    Edge Locate(const T& element) const {
        auto pred = head_;
        auto curr = head_->next_.load();
        while (curr->element_ < element) {
            pred = curr;
            curr = curr->next_.load();
        }
        return Edge{pred, curr};
    }

    bool Validate(const Edge& edge) const {
        return !edge.pred_->marked_ &&
               !edge.curr_->marked_ &&
               edge.pred_->next_ == edge.curr_;
    }

 private:
    ArenaAllocator& allocator_;
    Node* head_{nullptr};
    std::atomic<size_t> size;
};

template <typename T> using ConcurrentSet = OptimisticLinkedSet<T>;
