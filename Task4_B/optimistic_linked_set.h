#pragma once

#include <iostream>
#include <vector>
#include <atomic>
#include <algorithm>
#include <thread>
#include "arena_allocator.h"
#include<mutex>

class TicketSpinlock {
public:
    TicketSpinlock() {
        next_free_ticket.store(0);
        owner_ticket.store(0);
    }

    void lock() {
        const size_t this_thread_ticket = next_free_ticket.fetch_add(1);
        while (this_thread_ticket != owner_ticket.load()) {
            std::this_thread::yield();
        }
    }

    void unlock() {
        owner_ticket.fetch_add(1);
    }

private:
    std::atomic<size_t> next_free_ticket;
    std::atomic<size_t> owner_ticket;
};

template <typename T>
struct ElementTraits {
    static T Min() {
        return std::numeric_limits<T>::min();
    }
    static T Max() {
        return std::numeric_limits<T>::max();
    }
};

template <typename T>
class OptimisticLinkedSet {
 private:
    struct Node {
        T element_;
        std::atomic<Node*> next_;
        TicketSpinlock lock_{};
        std::atomic<bool> marked_{false};

        Node(const T& element, Node* next = nullptr)
            : element_(element),
              next_(next) {
        }
    };

    struct Edge {
        Node* pred_;
        Node* curr_;

        Edge(Node* pred, Node* curr)
            : pred_(pred),
              curr_(curr) {
        }
    };

 public:
    explicit OptimisticLinkedSet(ArenaAllocator& allocator)
        : allocator_(allocator), size(0) {
        CreateEmptyList();

    }

    bool Insert(const T& element) {
        while (true) {
            Edge edge = Locate(element);
            std::unique_lock<TicketSpinlock>(edge.pred_->lock_);
            std::unique_lock<TicketSpinlock>(edge.curr_->lock_);
            if (Validate(edge)) {
                if (edge.curr_->element_ == element) {
                    return false;
                }
                Node* new_node = allocator_.New<Node>(element, edge.curr_);
                size.fetch_add(1);
                edge.pred_->next_.store(new_node);
                return true;
            }
        }
    }

    bool Remove(const T& element) {
        while (true) {
            Edge edge = Locate(element);
            std::unique_lock<TicketSpinlock>(edge.pred_->lock_);
            std::unique_lock<TicketSpinlock>(edge.curr_->lock_);
            if (Validate(edge)) {
                if (edge.curr_->element_ != element) {
                    return false;
                }
                edge.curr_->marked_ = true;
                edge.pred_->next_.store(edge.curr_->next_);
                size.fetch_sub(1);
                return true;
            }
        }
    }

    bool Contains(const T& element) const {
        while (true) {
            Edge edge = Locate(element);
            std::unique_lock<TicketSpinlock>(edge.pred_->lock_);
            std::unique_lock<TicketSpinlock>(edge.curr_->lock_);
            if (Validate(edge)) {
                bool res = edge.curr_->element_ == element;
                return res;
            }
        }
    }

    size_t Size() const {
        return size;
    }

 private:
    void CreateEmptyList() {
        head_ = allocator_.New<Node>(ElementTraits<T>::Min());
        head_->next_ = allocator_.New<Node>(ElementTraits<T>::Max());
    }

    Edge Locate(const T& element) const {
        auto pred = head_;
        auto curr = head_->next_.load();
        while (curr->element_ < element) {
            pred = curr;
            curr = curr->next_.load();
        }
        return Edge{pred, curr};
    }

    bool Validate(const Edge& edge) const {
        return !edge.pred_->marked_ &&
               !edge.curr_->marked_ &&
               edge.pred_->next_ == edge.curr_;
    }

 private:
    ArenaAllocator& allocator_;
    Node* head_{nullptr};
    std::atomic<size_t> size;
};

template <typename T> using ConcurrentSet = OptimisticLinkedSet<T>;

